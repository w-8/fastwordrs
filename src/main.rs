#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")] // hide console window on Windows in release
mod uidesign;
mod worddata;
const APP_ICON: &[u8] = include_bytes!("..\\fa1.ico");

fn main() {

    let icon = image::load_from_memory(APP_ICON).unwrap();

    let native_options = eframe::NativeOptions{
        icon_data:Some(eframe::IconData{
            width: icon.width(),
            height: icon.height(),
            rgba: icon.into_bytes(),
        }),
        ..Default::default()
    };
    eframe::run_native("速词 V1.1.1", native_options, Box::new(|cc| Box::new(uidesign::MyEguiApp::new(cc))));
}